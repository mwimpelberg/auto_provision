#!/bin/bash

vm_name=$1
ip_addr=$2


ssh root@masterdns "sed -e 's/HOST/$vm_name/g' /var/www/html/anaconda-ks.cfg-bak > /var/www/html/anaconda-ks.cfg"
result=$(ssh root@masterdns "ls -ltra /var/www/html/anaconda-ks.cfg | grep -v BAK | wc -l")
if [ $result -eq 0 ]; then
	exit 1
	echo "FAILURE"
fi
ssh root@masterdns "chmod 777 /var/www/html/anaconda-ks.cfg"
/usr/bin/ansible-playbook vsphere.yaml -e new_vm=$vm_name 
mac=$(/usr/bin/ansible-playbook getmac.yaml -e new_vm=$vm_name | grep macaddress | grep -v dash | awk '{print $NF}' | tr "\" " " " | awk '{print $1}')
last_octet=$(echo $ip_addr | awk -F "." '{print $NF}')
/usr/bin/ansible-playbook dnsdhcp.yaml -e "vm_name=$vm_name mac=$mac ip_addr=$ip_addr last_octet=$last_octet"


